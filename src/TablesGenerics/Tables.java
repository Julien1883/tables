package TablesGenerics;

public class Tables implements Comparable<Tables> {
    private String name;
    private String numberOfChairs;
    private String color;
    private String length;
    private String width;

    public Tables(String name, String numberOfChairs, String color, String length, String width) {
        this.name = name;
        this.numberOfChairs = numberOfChairs;
        this.color = color;
        this.length = length;
        this.width = width;
    }

    @Override
    public String toString() {
        return "Tables{" +
                "name='" + name + '\'' +
                ", numberOfChairs='" + numberOfChairs + '\'' +
                ", color='" + color + '\'' +
                ", length='" + length + '\'' +
                ", width='" + width + '\'' +
                '}';
    }

    @Override
    public int compareTo(Tables o) {
        return this.color.compareTo(o.getColor());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberOfChairs() {
        return numberOfChairs;
    }

    public void setNumberOfChairs(String numberOfChairs) {
        this.numberOfChairs = numberOfChairs;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object obj) {

        Tables anotherTable = (Tables) obj;
        return this.getName().equals(anotherTable.getName());
    }


    @Override
    public int hashCode() {
        return name.hashCode();
    }


    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}