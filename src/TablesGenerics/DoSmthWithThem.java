package TablesGenerics;

import java.util.*;

public class DoSmthWithThem {


    ArrayList<Tables> tablesList = new ArrayList<>();

    public static void main(String[] args) {
        DoSmthWithThem temp = new DoSmthWithThem();

        temp.run();

    }

    private void run() {
        tablesList = splitOurTables();


        Collections.sort(tablesList);
        System.out.println("tables list by name");
        System.out.println(tablesList);
        HashSet<Tables> songHashSet = new HashSet<>(tablesList);
        System.out.println("pure set");
        System.out.println(songHashSet);
        System.out.println("tree set by name (compareTo at E)");
        TreeSet<Tables> songTreeSet = new TreeSet<>(songHashSet);
        System.out.println(songTreeSet);

        TreeSet<Tables> treeSetWithComparatorBySurfaseArea = new TreeSet<>(new TablesBySurfaseArea());
        treeSetWithComparatorBySurfaseArea.addAll(songTreeSet);
        System.out.println("tree set with comparator by Surface Area");
        System.out.println(treeSetWithComparatorBySurfaseArea);

    }

    private ArrayList<Tables> splitOurTables() {
        String tables = getTables();
        String[] rawStrings = tables.split("\n");
        ArrayList<Tables> tableList = new ArrayList<>();

        for (String rawString : rawStrings) {
            String[] split = rawString.split("/");
            tableList.add(new Tables(split[0], split[1], split[2], split[3], split[4]));
        }
        return tableList;
    }


    private String getTables() {
        return "DANDERYD/4/White/130/80\n" +
                "GAMLEBY/4/Fair/134/78\n" +
                "GAMLEBY/4/Fair/134/78\n" +
                "GAMLEBY/4/Fair/134/78\n" +
                "GAMLEBY/4/Fair/134/78\n" +
                "SKOGSTA/6/Black/235/100\n" +
                "NORDVIKEN/6/Wood/223/95\n" +
                "MELLTORP/2/White/75/75\n" +
                "MELLTORP/2/White/75/75\n" +
                "MELLTORP/2/White/75/75\n" +
                "MELLTORP/2/White/75/75\n" +
                "ALLSTA/0/Black/13/6\n" +
                "LERHAMN/0/White/118/74\n" +
                "LERHAMN/0/White/118/74\n" +
                "GAMLARED/2/Black/17/5\n";
    }



    class TablesByNameComparator implements Comparator<Tables> {
        @Override
        public int compare(Tables o1, Tables o2) {
            return o1.getName().compareTo(o2.getName());
        }

    }

    class TablesBySurfaseArea implements Comparator<Tables> {
        @Override
        public int compare(Tables o1, Tables o2) {
            int length1 =Integer.parseInt(o1.getLength());
            int length2 =Integer.parseInt(o2.getLength());
            int width1 =Integer.parseInt(o1.getWidth());
            int width2 =Integer.parseInt(o2.getWidth());

            if((length1*width1)==(length2*width2)){
                return 1;
            }
            return 0;
        }

    }
}
